extends Node

var duration = 0.0
var pitch = 0.0

func _init(pitch_arg, duration_arg):
	pitch = pitch_arg
	duration = duration_arg

func is_chord():
	if typeof(pitch) == TYPE_ARRAY:
		return true
	return false

func get_pitch():
	return pitch

func get_duration():
	return duration
