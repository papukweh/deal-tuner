extends Node
var scale

var prev = 0
var dist = [0.75, 0.2, 0.05]
var momentum = 1

func _init(type, root):
	scale = []
	for oct in SYNTH.octaves:
		var prev = SYNTH.notes.find(root)
		var sequence = SYNTH.scales[type]
		for i in range(len(sequence)-1):
			var step =  sequence[i]
			var new_note = prev + step
			scale.append(SYNTH.get_note(SYNTH.notes[new_note], oct))
			prev = new_note

func set_first_note(note):
	if note < len(scale):
		prev = note
	else:
		print("Nota errada")

func set_probabilities(prob):
	if prob.sum() == 1.0:
		dist = prob
	else:
		print("Probabilidades erradas")

func keep_probability():
	return dist[0]

func shift_probability():
	return dist[1]

func jump_probability():
	return dist[2]

func get_chord(octave, root, inv):
	root = SYNTH.notes.find(root) + octave*12
	var ret = []
	if inv == 0:
		ret = [scale[root], scale[root+2], scale[root+4]]
	elif inv == 1:
		ret = [scale[root+2], scale[root+4], scale[root+7]]
	else:
		ret = [scale[root+4], scale[root+7], scale[root+9]]
	return ret

func get_random_note():
	var next = prev
	var rand = randf()
	if rand <= jump_probability():
		next = rand_range(0,len(scale))
	elif rand <= shift_probability():
		momentum = momentum * -1
		next = prev + momentum * rand_range(0,7)
	else:
		next = prev + momentum * rand_range(0,7)
	return scale[int(next)]

var chord_progression = ["C", "G", "A", "F"]
var prev_chord = 0

func get_random_chord(octave, inv):
	var chord = prev_chord
	var rand = randf()
	if rand <= 0.1:
		chord = rand_range(0, len(chord_progression))
	elif rand <= 0.35:
		chord = prev_chord
	else:
		chord = (prev_chord + 1) % len(chord_progression)
	return get_chord(octave, chord_progression[chord], inv)
