extends Node

var Scale = load("res://Audio/Scale.gd")
var Note = load("res://Audio/Note.gd")

var Beats = [-1, 0.25, 0.5, 1, 1.5, 2, 3, 4]

var BassBeats = [8, 16]

func create_melody_track(type, key):
	print("Criando um melody track")
	var scale = Scale.new(type, key)
	scale.set_first_note(20)
	var track = []
	for i in range(100):
		var tempo = Beats[rand_range(0,len(Beats))]
		track.append(Note.new(scale.get_random_note(), tempo))
	return track

func create_rhythm_track(type, key):
	print("Criando um rhythm track")
	var scale = Scale.new(type, key)
	var track = []
	var init = 0
	var tempo = 0
	for i in range(100):
		if (i - init) >= tempo:
			tempo = BassBeats[rand_range(0,len(BassBeats))]
			var inversion = rand_range(0,3)
			track.append(Note.new(scale.get_random_chord(-2, inversion), tempo))
			init = i
		else:
			track.append(Note.new(null, -1))
	return track


func create_song(type, key, num_tracks):
	print("Criando uma song")
	var song = []
	song.append(create_melody_track(type, key))
	song.append(create_rhythm_track(type, key))
	return song
