#helper for multiple audio stream playback
extends AudioStreamPlayer

func play_note(pitch, duration):
	if duration > 0:
		var asp = self.duplicate(DUPLICATE_USE_INSTANCING)
		get_parent().add_child(asp)
		asp.stream = stream
		asp.pitch_scale = pitch
		print(pitch)
		asp.play()
		var tween = Tween.new()
		get_parent().add_child(tween)
		tween.interpolate_property(asp, "volume_db",
		0, -20, duration,
		Tween.TRANS_LINEAR, Tween.EASE_OUT)
		tween.start()
		yield(tween, "tween_all_completed")
		tween.queue_free()
		asp.queue_free()

func play_chord(chord, duration):
	for c in chord:
		play_note(c, duration)

func play( from_position=0.0 ):
	if !playing:
		.play(from_position)
	else:
		var asp = self.duplicate(DUPLICATE_USE_INSTANCING)
		get_parent().add_child(asp)
		asp.stream = stream
		asp.play()
		yield(asp, "finished")
		asp.queue_free()
