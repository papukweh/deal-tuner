extends Node
var octaves = [-3, -2, -1, 0, 1]
var notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]

var scales = {"Major": [2, 2, 1, 2, 2, 2, 1], 
			  "Minor": [2, 1, 2, 2, 1, 2, 2], 
			  "PentatonicMajor": [2, 2, 3, 2, 3]}

func get_freq(note, octave):
	var n = -9 + (octave*12) + (notes.find(note))
	return 440.0 * pow(2.0,(n/12.0))

func get_pitch(freq):
	return 69 + 12*(log(freq/440.0)/log(2))

func get_note(note, octave):
	return get_pitch(get_freq(note, octave)) / get_pitch(get_freq("A", 0))
