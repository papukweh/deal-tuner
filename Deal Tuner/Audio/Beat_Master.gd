extends Node

export(float) var BPM = 90.0
onready var time = 0.0
onready var beat = 0

var audio = null
var current_song = null

var playing = false
var loop = false

# Called when the node enters the scene tree for the first time.
func _ready():
	audio = get_node("Streams")
	rand_seed(13)
	var song = $Composer.create_song("Major", "C", 2)
	play_song(song)

func play_song(song):
	var count = 1
	for track in len(song) - 1:
		var asp = audio.get_node("Track0").duplicate(DUPLICATE_SCRIPTS)
		asp.name = "Track"+str(count)
		audio.add_child(asp)
		count += 1
	current_song = song
	playing = true
	time = 0.0
	beat = 0

func is_playing():
	return playing

func get_bpm():
	return BPM

func set_bpm(value):
	BPM = value

func get_beat_duration():
	return 60.0/BPM

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if is_playing():
		time += delta
		if time >= get_beat_duration():
			for track in range(len(current_song)):
				var stream = audio.get_node("Track"+str(track))
				var note = current_song[track][beat]
				if note.is_chord():
					stream.play_chord(note.get_pitch(), get_beat_duration()*note.get_duration())
				else:
					stream.play_note(note.get_pitch(), get_beat_duration()*note.get_duration())
			time = 0.0
			beat = beat + 1
			if beat >= len(current_song[0]):
				print("Acabou!")
				if loop:
					beat = 0
				else:
					playing = false
