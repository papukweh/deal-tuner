extends KinematicBody2D

const SPEED = 500.0
var velocity

func _physics_process(delta):
	velocity = Vector2(0,0)
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	elif Input.is_action_pressed("ui_up"):
		velocity.y -= 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	elif Input.is_action_pressed("ui_right"):
		velocity.x += 1
	move_and_slide(velocity*SPEED)
